<h1 align="center"> Hi there 👋 </h1> 

<p align="center"> My name is Michelle Mello. I'm a Technical Documentation Analyst in a brazilian Multinational Corporation. Currently, I'm studying UX Design and Front-end developement.</p>
<p align="center">Hope to get better and better each day. Feel free to reach me always. Looking forward to exchange knowledge.</p>

##
<div align="center"
<ul>
  <p> ✍️ UX Writer and Technical Writer</p>
  <p> 🤖 HTML5, CSS3 and Javascript</p>  
   <p> 😄 Pronouns: she/her </p>
</ul> 
</div>

<div align="center" style="display: inline_block"><br>
<img align="center" alt="Michelle-HTML" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" />
<img align="center" alt="Michelle-CSS" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />
<img align="center" alt="Michelle-Js" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" />
</div>

 
##
<div align="center" style="display: inline_block"><br>
  <div align="center">
    <a href="https://twitter.com/_michellemello" target="_blank"> <img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" target="_blank"> </a>
    <a href="https://www.linkedin.com/in/michelle-mello-18827b163/" target="_blank"> <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"> </a>
  </div>

<!--
##

<div align="center" style="display: inline_block"><br>
<div align="center">
  <img src="https://github-readme-stats.vercel.app/api?username=michellemello&show_icons=true&theme=radical&include_all_commits=true">
</div>

-->

